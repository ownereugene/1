### Создание проекта с помощью Vite
Чтобы [создать приложение Vite](https://vitejs.dev/guide/#scaffolding-your-first-vite-project), откройте терминал и перейдите в папку, в которой вы хотите сохранить программу Vite. Затем выполните эту команду:

    npm create vite@latest

Вам будет предложено выбрать структуру и вариант (шаблон). В нашем случае мы будем использовать React, поэтому выберите React.
![](./images/vbUIaENpM0OiHEWrRDlfnLmsUb83-2023-01-03T05 50 36.jpg)

Вы также можете напрямую указать шаблон, который хотите использовать, и название проекта в одной строке:

    npm create vite@latest my-react-app --template react

Примечание. **my-react-app** — это имя приложения Vite, которое мы хотим создать. Вы можете изменить его на любое имя, которое вы предпочитаете.

Затем выполните следующие команды в терминале

    cd my-react-app
    npm install
    npm run dev

Двигаемся дальше... Выполнение приведенной выше команды запустит сервер разработки. Затем откройте браузер и введите **http://localhost:3000**.

Вы должны увидеть логотип React со счетчиком и кнопкой, как показано ниже:
![](./images/vbUIaENpM0OiHEWrRDlfnLmsUb83-2023-01-03T05 50 36__.jpg)


### Создание проекта с помощью Create-React-App (CRA)

[Create React App](https://github.com/facebookincubator/create-react-app) — удобная среда для изучения React и лучший способ начать создание нового [одностраничного](https://ru.legacy.reactjs.org/docs/glossary.html#single-page-application) приложения на React.

    npx create-react-app my-app
    cd my-app
    npm start

Он создаст каталог с именем my-app внутри текущей папки.
Внутри этого каталога будет сгенерирована исходная структура проекта и установлены зависимости:

    my-app
        ├── README.md
        ├── node_modules
        ├── package.json
        ├── .gitignore
        ├── public
        │   ├── favicon.ico
        │   ├── index.html
        │   └── manifest.json
        └── src
        ├── App.css
        ├── App.js
        ├── App.test.js
        ├── index.css
        ├── index.js
        ├── logo.svg
        └── serviceWorker.js
        └── setupTests.js

![](./images/cra_1.png)

Вы также можете напрямую указать шаблон, который хотите использовать, и название проекта в одной строке:

    # Шаблон Redux на JS
    npx create-react-app my-app --template redux

    # Шаблон Redux на TypeScript
    npx create-react-app my-app --template redux-typescript

Затем выполните следующие команды в терминале

    cd my-app
    npm start

Выполнение приведенной выше команды запустит сервер разработки. Затем откройте браузер и введите http://localhost:3000.
Вы должны увидеть страницу с логотипом React:
![](./images/cra_2.png)
